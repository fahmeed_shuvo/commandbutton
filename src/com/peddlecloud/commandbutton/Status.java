package com.peddlecloud.commandbutton;

public interface Status {
    public boolean status();
}

class TurnOn implements Status
{
    public boolean status() {
        return true;
    }
}

class TurnOff implements Status
{
    public boolean status() {
        return false;
    }
}
