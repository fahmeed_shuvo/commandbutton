package com.peddlecloud.commandbutton;

public interface Devices {
    public void execute(String s);
    public void show();
    public String getDeviceName();
}
