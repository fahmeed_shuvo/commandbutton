package com.peddlecloud.commandbutton;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RemoteApp
{
    public static Map<String, Devices> devices= new HashMap<>();

    public void install()
    {
        devices.put("f", new DeviceWithParameter("Fan"));
        devices.put("a", new AC("AC"));
        devices.put("l", new DeviceWithoutParameter("Light"));
        devices.put("d", new AutoDoor("Auto Door"));
        System.out.println("l for light on/off\n" +
                "d for auto door open/close\n" +
                "f0 to turn fan on/off, f+/f- to control fan speed\n" +
                "a0 to turn ac on/off, at+/at- to control temperature, as+/as- for speed");
    }

    static void run(String c)
    {
        if (c.toLowerCase().equals("show"))
        {
            for (Map.Entry<String, Devices> device: devices.entrySet() ) {
                device.getValue().show();
            }
        }
        else
        {
            try
            {
                Devices device = devices.get(c.toLowerCase().substring(0,1));
                device.execute(c.substring(1));
            }
            catch (Exception e)
            {
                System.out.println("Invalid input. try again.");
            }
        }
    }
}