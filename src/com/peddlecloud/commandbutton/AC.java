package com.peddlecloud.commandbutton;

import org.jetbrains.annotations.NotNull;

public class AC extends DeviceWithoutParameter {
    private int temperatureStatus = 0;
    private Speed speed;
    private static final int MIN = 16;
    private static final int MAX = 32;

    AC(String name) {
        super(name);
    }

    private void tempControl(char c) {
        if (c == '+') {
            if (temperatureStatus < MAX)
                temperatureStatus++;
            else {
                System.out.println("Temperature is already maximum.");
            }
        } else if (c == '-') {
            if (temperatureStatus > MIN)
                temperatureStatus--;
            else if (temperatureStatus > 0) {
                System.out.println("Temperature is already minimum.");
            }
        }
    }

    private void toggleAC() {
        toggleDevice();
        temperatureStatus = getDeviceStatus().status() ? MIN : 0;
        speed = getDeviceStatus().status() ? Speed.low : null;
    }

    private void speedControl(char c) {
        if (c == '+')
            speed = speed.next();
        else if (c == '-')
            speed = speed.previous();
    }

    public void show() {
        System.out.println(getDeviceName() + " is now " +
                (getDeviceStatus().status() ?
                        (temperatureStatus + "c " + speed + " speed") : "off"));
    }

    @Override
    public void execute(@NotNull String s) {
        switch (s.charAt(0)) {
            case '0':
                toggleAC();
                break;
            case 't':
                tempControl(s.charAt(1));
                break;
            case 's':
                speedControl(s.charAt(1));
                break;
            default:
                System.out.println("Invalid input. try again.");
                break;
        }
        show();
    }
}
