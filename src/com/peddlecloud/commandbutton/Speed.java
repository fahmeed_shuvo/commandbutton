package com.peddlecloud.commandbutton;

public enum Speed {
    low, mid, high;
    private static Speed[] speeds = values();

    public Speed next() {
        try {
            return speeds[this.ordinal() + 1];
        } catch (Exception e) {
            System.out.println("Can't get more than high.");
            return this;
        }
    }

    public Speed previous() {
        try {
            return speeds[this.ordinal() - 1];
        } catch (Exception e) {
            System.out.println("Can't get less than low.");
            return this;
        }
    }
}