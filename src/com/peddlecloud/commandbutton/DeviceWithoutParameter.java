package com.peddlecloud.commandbutton;

import org.jetbrains.annotations.NotNull;

public class DeviceWithoutParameter implements Devices {
    private String deviceName;
    private Status deviceStatus;

    DeviceWithoutParameter(String name)
    {
        deviceName = name;
        deviceStatus = () -> false;
    }
    public String getDeviceName() {
        return deviceName;
    }
    Status getDeviceStatus() {
        return deviceStatus;
    }
    void setDeviceStatus(Status getDeviceStatus) {
        deviceStatus = getDeviceStatus;
    }
    public void toggleDevice()
    {
        if (deviceStatus.status())
            deviceStatus = new TurnOff();
        else
            deviceStatus = new TurnOn();
    }

    @Override
    public void show() {
        System.out.println(deviceName + " is now " + (deviceStatus.status() ? "on" : "off"));
    }

    @Override
    public void execute(String s) {
        toggleDevice();
        show();
    }
}
