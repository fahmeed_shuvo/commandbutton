package com.peddlecloud.commandbutton;

import org.jetbrains.annotations.NotNull;

public class DeviceWithParameter extends DeviceWithoutParameter {
    private Speed speed = null;

    DeviceWithParameter(String name) {
        super(name);
    }

    public void show() {
        System.out.println(getDeviceName() + " is now "
                + (getDeviceStatus().status() ? speed : "off"));
    }

    public void setDevice() {
        toggleDevice();
        speed = getDeviceStatus().status() ? Speed.low : null;
    }

    public void toggleDevice(String s) {
        switch (s.charAt(0)) {
            case '0':
                setDevice();
                break;
            case '+':
                speed = speed.next();
                break;
            case '-':
                speed = speed.previous();
                break;
            default:
                System.out.println("Invalid input. try again.");
        }
    }

    @Override
    public void execute(String s) {
        toggleDevice(s);
        show();
    }
}
