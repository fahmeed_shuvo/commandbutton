package com.peddlecloud.commandbutton;

import java.util.Scanner;

class AutoDoorTest {
    public static void main(String[] args) {
        Devices auto = new AutoDoor("Auto Door");
        while(true)
        {
            auto.execute(new Scanner(System.in).next());
        }
    }
}