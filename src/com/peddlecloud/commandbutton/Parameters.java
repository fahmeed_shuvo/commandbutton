package com.peddlecloud.commandbutton;

public class Parameters {
    private String[] parameters = {"low", "mid", "high", null};

    public String getParameter() {
        return parameter;
    }

    public void setParameter(int i) {
        this.parameter = parameters[i];
    }

    private String parameter;

    public String next(int i)
    {
        try
        {
            final String parameterFinal = parameters[i++];
        } catch (NullPointerException e)
        {
            System.out.println("Can't get more than high.");
        }
        return parameters[i];
    }

    public String prev(int i)
    {
        try
        {
            final String parameter = parameters[i--];
        } catch (NullPointerException e)
        {
            System.out.println("Can't get more than high.");
        }
        return parameters[i];
    }
}
