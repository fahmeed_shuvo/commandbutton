package com.peddlecloud.commandbutton;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        RemoteApp remote = new RemoteApp();
        remote.install();

        while(true)
        {
            remote.run(scanner.next());
        }
    }
}
