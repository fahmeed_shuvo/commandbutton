package com.peddlecloud.commandbutton;

import org.jetbrains.annotations.NotNull;

import java.util.Timer;
import java.util.TimerTask;

public class AutoDoor extends DeviceWithoutParameter {
    int time = 5;
    Timer timer = new Timer();

    class RemindTask extends TimerTask {
        public void run() {
            System.out.println(getDeviceName() + " closed!");
            setDeviceStatus(new TurnOff());
            timer.cancel();
            timer.purge();
            timer = new Timer();
        }
    }

    AutoDoor(String name) {
        super(name);
    }

    @Override
    public void toggleDevice() {
        if (getDeviceStatus().status())
        {
            setDeviceStatus(new TurnOff());
            timer.cancel();
            timer.purge();
            timer = new Timer();
        }
        else
        {
            setDeviceStatus(new TurnOn());
            timer.schedule(new RemindTask(), time * 1000);
        }
    }

    @Override
    public void show() {
        System.out.println(getDeviceName() + " is now "
                + (getDeviceStatus().status() ? "open for " + time + " seconds." : "closed."));
    }

    @Override
    public void execute(@NotNull String s) {
        toggleDevice();
        show();
    }
}
