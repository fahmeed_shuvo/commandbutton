package practice.java.generics;

public class GenericsTest {
    public static void main(String[] args)
    {
        //Pair&Util test scenario
        Pair<Integer, String> p1 = new Pair<>(1, "apple");
        Pair<Integer, String> p2 = new Pair<>(1, "apple");
        //Pair<Integer, String> p2 = new Pair<>(2, "pear");
        boolean same = Util.compare(p1, p2);
        if (same)
        {
            System.out.println("Same");
        }
        else
        {
            System.out.println("Not same");
        }

        //generics1 test scenario
        Generics1<Integer> generics1 = new Generics1<>(10);
        Generics1<String> generics2 = new Generics1<>("Hello");
        generics1.operationKey();
        generics2.operationKey();
    }
}
