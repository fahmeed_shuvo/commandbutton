package practice.java.generics;

import java.util.*;

public class Freq {
    public static void main(String[] args) {
        Map<String, Integer> m = new TreeMap<>();
        String[] strings = new String[] {
                "if", "it", "is", "to", "be", "it", "is", "up", "to", "me", "to", "delegate"
        };

        // Initialize frequency table from command line
        for (String a : strings) {
            Integer freq = m.get(a);
            m.put(a, (freq == null) ? 1 : freq + 1);
        }

        System.out.println(m.size() + " distinct words:");
        System.out.println(m);
    }
}