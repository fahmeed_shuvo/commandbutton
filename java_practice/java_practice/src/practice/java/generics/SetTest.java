package practice.java.generics;

import java.util.*;
import java.util.stream.Collectors;

public class SetTest<S> {
    public static void main(String[] args)
    {
        String[] s1 = new String[]{"I", "came", "I", "saw", "I", "left"};
        Set<String> dups = new LinkedHashSet<String>();
        Set<String> unique = new LinkedHashSet<String>();

        //foreach
        for (String s : s1) {
            if (!unique.add(s))
            {
                dups.add(s);
            }
        }
        unique.removeAll(dups);
        System.out.println(dups.size() + " Unique words: " + unique);
        System.out.println(dups.size() + " Duplicate words: " + dups);
        //System.out.println(dups.size() + " distinct words: " + dups);

        //aggregate operations
        Set<String> distinctWords = Arrays.asList(s1).stream().collect(Collectors.toSet());
        System.out.println(distinctWords.size() + " distinct words: " + distinctWords);
    }
}
