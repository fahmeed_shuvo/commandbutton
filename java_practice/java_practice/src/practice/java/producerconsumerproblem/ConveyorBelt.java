package practice.java.producerconsumerproblem;

import java.util.LinkedList;
import java.util.Queue;

public class ConveyorBelt {

    private Queue<String> queue = new LinkedList<>();
    private int beltSize;
    private String name;

    public ConveyorBelt(String name, int beltSize) {
        this.name = name;
        this.beltSize = beltSize;
    }

    public int getCurrentSize() {
        return queue.size();
    }

    public synchronized Queue<String> getQueue() {
        return queue;
    }

    public synchronized void printQueue()
    {
        System.out.println(name + " belt" + queue);
    }

    public synchronized void put(String type){
        queue.add(type);
    }

    public synchronized boolean isNotFull()
    {
        if (queue.size() == beltSize)
            System.out.println(name + " cake belt is full.");
        return queue.size() < beltSize;
    }

    public synchronized String pop() {
        return this.queue.poll();
    }
}
