package com.peddlecloud.taxcollectionprogram;

public interface Managers {
    public void insertData();
    public void updateStatus(TaxPayee payee);
    public Manager popQueue();
}
