package com.peddlecloud.taxcollectionprogram;

public interface TaxPayee {
    public boolean isEligible();
    public void updateTaxPayer(String name, int age, long Value);
}
