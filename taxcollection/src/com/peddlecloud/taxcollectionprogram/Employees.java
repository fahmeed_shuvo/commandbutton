package com.peddlecloud.taxcollectionprogram;

public interface Employees {
    public boolean checkStatus(TaxPayee payee);
    public boolean checkEligibility(TaxPayee payee);
    public void setQueue(TaxPayee payee);
}
