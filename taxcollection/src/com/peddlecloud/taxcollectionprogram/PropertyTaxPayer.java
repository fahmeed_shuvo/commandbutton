package com.peddlecloud.taxcollectionprogram;

public class PropertyTaxPayer implements TaxPayee {
    private String name;
    private int age;
    private long propertyValue;
    private boolean taxStatus = false;

    public PropertyTaxPayer(String name, int age, long propertyValue) {
        this.name = name;
        this.age = age;
        this.propertyValue = propertyValue;
    }

    public void updateTaxPayer(String name, int age, long propertyValue) {
        this.name = name;
        this.age = age;
        this.propertyValue = propertyValue;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public long getAssetValue() {
        return propertyValue;
    }

    public boolean isEligible() {
        return (age < 20 && propertyValue >= 10000000)
                || (age >50 && propertyValue >= 30000000);
    }

    public void updateTaxStatus(boolean taxStatus) {
        this.taxStatus = taxStatus;
    }
}
