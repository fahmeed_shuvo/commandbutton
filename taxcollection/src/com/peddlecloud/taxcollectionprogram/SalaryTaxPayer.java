package com.peddlecloud.taxcollectionprogram;

public class SalaryTaxPayer implements TaxPayee {
    private String name;
    private int age;
    private long annualSalary;
    private boolean taxStatus = false;

    public SalaryTaxPayer(String name, int age, long annualSalary) {
        this.name = name;
        this.age = age;
        this.annualSalary = annualSalary;
    }

    public void updateTaxPayer(String name, int age, long annualSalary) {
        this.name = name;
        this.age = age;
        this.annualSalary = annualSalary;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public long getAssetValue() {
        return annualSalary;
    }

    public boolean isEligible() {
        return age >= 20 && age <=50 && annualSalary >= 3000000;
    }

    public void updateTaxStatus(boolean taxStatus) {
        this.taxStatus = taxStatus;
    }
}
